# MPC5744P Timer & ADC

This project will show you how to access timer and adc hardware of MPC5744P.
Please note that this project is inherited from the built-in example of S32D.

#### ```/src``` folder:  
This folder contains the firmware and main.c files.
The adc.c file originally does NOT exist in the original official example.

#### ```/include``` folder:
This folder contains some necessary header files. adc.h and project.h does not exist in the original example.
